import React from 'react'
import HomeFeatures from './components/HomeFeatures'
import HomeFeaturesProduct from './components/HomeFeaturesProduct'

class Home extends React.Component{
	render(){
		return ( 
			<div>
			    <HomeFeatures/>
			    <HomeFeaturesProduct/>
			</div> 
		)
	}
}

export default {
	component : Home
}