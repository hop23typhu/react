import React from 'react'

var HomeFeaturesProduct = React.createClass({
  render: function() {
    return (

      <div id="fh5co-feature-product" className="fh5co-section-gray">
        <div className="container">
          <div className="row">
            <div className="col-md-12 text-center heading-section">
              <h3>Giving is Virtue.</h3>
              <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
            </div>
          </div>
          <div className="row row-bottom-padded-md">
            <div className="col-md-12 text-center animate-box">
              <p><img src="images/cover_bg_1.jpg" alt="Free HTML5 Bootstrap Template" className="img-responsive" /></p>
            </div>
            <div className="col-md-6 text-center animate-box">
              <p><img src="images/cover_bg_2.jpg" alt="Free HTML5 Bootstrap Template" className="img-responsive" /></p>
            </div>
            <div className="col-md-6 text-center animate-box">
              <p><img src="images/cover_bg_3.jpg" alt="Free HTML5 Bootstrap Template" className="img-responsive" /></p>
            </div>
          </div>
          <div className="row">
            <div className="col-md-4">
              <div className="feature-text">
                <h3>Love</h3>
                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
              </div>
            </div>
            <div className="col-md-4">
              <div className="feature-text">
                <h3>Compassion</h3>
                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
              </div>
            </div>
            <div className="col-md-4">
              <div className="feature-text">
                <h3>Charity</h3>
                <p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.</p>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
});

export default HomeFeaturesProduct
