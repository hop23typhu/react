import React from 'react'
var HomeFeatures = React.createClass({
  render: function() {
    return (

      <div id="fh5co-features">
        <div className="container">
          <div className="row">
            <div className="col-md-4">
              <div className="feature-left">
                <span className="icon">
                  <i className="icon-profile-male" />
                </span>
                <div className="feature-copy">
                  <h3>Become a volunteer</h3>
                  <p>Facilis ipsum reprehenderit nemo molestias. Aut cum mollitia reprehenderit.</p>
                  <p><a href="#">Learn More</a></p>
                </div>
              </div>
            </div>
            <div className="col-md-4">
              <div className="feature-left">
                <span className="icon">
                  <i className="icon-happy" />
                </span>
                <div className="feature-copy">
                  <h3>Happy Giving</h3>
                  <p>Facilis ipsum reprehenderit nemo molestias. Aut cum mollitia reprehenderit.</p>
                  <p><a href="#">Learn More</a></p>
                </div>
              </div>
            </div>
            <div className="col-md-4">
              <div className="feature-left">
                <span className="icon">
                  <i className="icon-wallet" />
                </span>
                <div className="feature-copy">
                  <h3>Donation</h3>
                  <p>Facilis ipsum reprehenderit nemo molestias. Aut cum mollitia reprehenderit.</p>
                  <p><a href="#">Learn More</a></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
});

export default HomeFeatures
