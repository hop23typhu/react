import React from 'react'
import Header from '../../components/Header'
import Footer from '../../components/Footer'
import './CoreLayout.scss'
import '../../styles/core.scss'

export const CoreLayout = ({ children }) => (
  <div id="fh5co-wrapper">
	  <div id="fh5co-page">
	    <Header />
	    <div>
	      {children}
	    </div>
	    <Footer />
	  </div>
  </div>
)

CoreLayout.propTypes = {
  children : React.PropTypes.element.isRequired
}

export default CoreLayout
