import React from 'react'
import { IndexLink, Link } from 'react-router'

var HeaderHero = React.createClass({
  render: function() {
    return (

      <div className="fh5co-hero">
        <div className="fh5co-overlay" />
        <div className="fh5co-cover text-center" data-stellar-background-ratio="0.5" style={{backgroundImage: 'url(images/cover_bg_1.jpg)'}}>
          <div className="desc animate-box">
            <h2><strong>Donate</strong> for the <strong>Poor Children</strong></h2>
            <span>HandCrafted by <a href="http://frehtml5.co/" target="_blank" className="fh5co-site-name">FreeHTML5.co</a></span>
            <span><a className="btn btn-primary btn-lg" href="#">Donate Now</a></span>
          </div>
        </div>
      </div>
    );
  }
});

export default HeaderHero
