import React from 'react'
import HeaderTop from './HeaderTop'
import HeaderSection from './HeaderSection'
import HeaderHero from './HeaderHero'
class Header extends React.Component{
	render(){
		return (
			<div>
				<HeaderTop/>
				<HeaderSection/>
				<HeaderHero/>
			</div>
		)
	}
}
export default Header