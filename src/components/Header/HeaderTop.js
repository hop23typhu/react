import React from 'react'
import { IndexLink, Link } from 'react-router'

export const HeaderTop = () => (
  <div className="header-top">
    <div className="container">
      <div className="row">
        <div className="col-md-6 col-sm-6 text-left fh5co-link">
          <a href="#">FAQ</a>
          <a href="#">Forum</a>
          <a href="#">Contact</a>
        </div>
        <div className="col-md-6 col-sm-6 text-right fh5co-social">
          <a href="#" className="grow"><i className="icon-facebook2" /></a>
          <a href="#" className="grow"><i className="icon-twitter2" /></a>
          <a href="#" className="grow"><i className="icon-instagram2" /></a>
        </div>
      </div>
    </div>
  </div>
)

export default HeaderTop
